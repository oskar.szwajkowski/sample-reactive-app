package com.example.reactive.services;

import com.example.reactive.model.Event;
import org.springframework.data.mongodb.core.ChangeStreamEvent;
import org.springframework.data.mongodb.core.ChangeStreamOptions;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.schema.MongoJsonSchema;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.query.Criteria.where;

@Service
public class EventService {

	private Flux<Event> eventChangeStream;
	private ReactiveMongoTemplate reactiveMongoTemplate;

	public EventService(ReactiveMongoTemplate reactiveMongoTemplate) {
		this.reactiveMongoTemplate = reactiveMongoTemplate;

		eventChangeStream = reactiveMongoTemplate
				.changeStream(newAggregation(match(where("eventType").in("ERROR", "WARN", "INFO", "DEBUG"))),
						Event.class,
						ChangeStreamOptions.builder().returnFullDocumentOnUpdate().build(),
						"documents1")
				.map(ChangeStreamEvent::getBody)
				.publish()
				.autoConnect(1);
	}

	public Flux<Event> getChangeStream() {
		return eventChangeStream;
	}


}
