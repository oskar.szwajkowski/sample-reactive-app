package com.example.reactive.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "documents1")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Event {

	@Id
	private String id;
	private EVENT_TYPE eventType;
	private String message;
}
