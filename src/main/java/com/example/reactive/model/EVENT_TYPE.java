package com.example.reactive.model;

public enum EVENT_TYPE {
	DEBUG, INFO, WARN, ERROR
}
