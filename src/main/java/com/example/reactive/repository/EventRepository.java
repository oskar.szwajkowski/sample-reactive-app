package com.example.reactive.repository;

import com.example.reactive.model.EVENT_TYPE;
import com.example.reactive.model.Event;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface EventRepository extends ReactiveCrudRepository<Event, String> {

	Flux<Event> findEventsByEventType(EVENT_TYPE type);
}
