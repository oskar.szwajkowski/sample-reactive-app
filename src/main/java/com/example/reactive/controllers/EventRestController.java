package com.example.reactive.controllers;

import com.example.reactive.model.EVENT_TYPE;
import com.example.reactive.model.Event;
import com.example.reactive.repository.EventRepository;
import com.example.reactive.services.EventService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;

@RestController
//@CrossOrigin("http://localhost:4200")
public class EventRestController {

	private EventRepository eventRepository;
	private EventService eventService;

	public EventRestController(EventRepository eventRepository, EventService eventService) {
		this.eventRepository = eventRepository;
		this.eventService = eventService;
	}

	@GetMapping("/events")
	public Flux<Event> getAllEvents(@RequestParam(defaultValue = "0") Long delay) {
		return eventRepository.findAll().take(30).delayElements(Duration.ofSeconds(delay));
	}

	@GetMapping("/events/info")
	public Flux<Event> getInfoEvents() {
		return eventRepository.findEventsByEventType(EVENT_TYPE.INFO);
	}

	@GetMapping("/events/debug")
	public Flux<Event> getDebugEvents() {
		return eventRepository.findEventsByEventType(EVENT_TYPE.DEBUG);
	}

	@GetMapping("/events/warn")
	public Flux<Event> getWarnEvents() {
		return eventRepository.findEventsByEventType(EVENT_TYPE.WARN);
	}

	@GetMapping("/events/error")
	public Flux<Event> getErrorEvents() {
		return eventRepository.findEventsByEventType(EVENT_TYPE.ERROR);
	}

	@GetMapping("/events/updates")
	public Flux<Event> getUpdates() {
		return eventService.getChangeStream();
	}

	@PostMapping("/events")
	public Mono<Event> addEvent(@RequestBody Event event) {
		System.out.println("adding event: " + event);
		return this.eventRepository.save(event);
	}

	@PostMapping("/events/async")
	public Mono<Void> addEventAsync(@RequestBody Event event) {
		System.out.println("adding event async: " + event);
		return this.eventRepository.save(event).subscribeOn(Schedulers.elastic()).then();
	}
}
